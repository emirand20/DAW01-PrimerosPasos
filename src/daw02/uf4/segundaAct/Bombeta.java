package uf4.segundaAct;

public class Bombeta {

    private int intensidad;

    public Bombeta(){
        this.intensidad = 0;
    }

    public int getIntensidad() {
        return intensidad;
    }

    public void setIntensidad(int intensidad) {
        this.intensidad = intensidad;
    }

    @Override
    public String toString() {
        return "Bombeta [intensidad=" + intensidad + "]";
    }    

    
  
}
    