package uf4.poo.clasesYObjetos;

public class Bicicleta {
    //Atributos
    String frenos;
    String tipoBici;
    String marca;

    //Metodos
    void tipoFrenos(String newFrenos) {
        frenos = newFrenos;
    }

    void numRuedas(String newTipoBici) {
        tipoBici = newTipoBici;
    }

    void tipoMarca(String newMarca){
        marca = newMarca;
    }


    public static void main(String[] args) {
        Bicicleta bicicleta1 = new Bicicleta();
        Bicicleta bicicleta2 = new Bicicleta();

        bicicleta1.frenos = "Disco";
        bicicleta1.tipoBici = "Montaña";
        bicicleta1.marca = "Orbea";

        bicicleta2.frenos = "Pasta";
        bicicleta2.tipoBici = "Carretera";
        bicicleta2.marca = "Trek";
    }
}