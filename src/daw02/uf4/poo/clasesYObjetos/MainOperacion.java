package uf4.poo.clasesYObjetos;

import java.util.Scanner;


public class MainOperacion {
    public static void main(String[] args) {
        Operacion op = new Operacion();
        /*
         * op.leerNums(); op.sumar(); op.resta(); op.multiplicacion(); op.division();
         * op.mostrarResultados();
         */
        Scanner n = new Scanner(System.in);
        System.out.println("Dime el primer numero: ");
        int n1 = n.nextInt();
        System.out.println("Dime el segundo numero: ");
        int n2 = n.nextInt();
        
        //Argumentos
        System.out.println("La suma es: " + op.sumar(n1,n2));
        int resta = op.resta(n1,n2);
        int mult = op.multiplicacion(n1,n2);
        int div = op.division(n1,n2);
        //op.mostrarResultados(suma, resta, mult, div);
    }
}
