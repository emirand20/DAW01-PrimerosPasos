package uf4.herenciaII.tpv;

public class Producte {
    String nom;
    int preu;
    String marca;
    String fechaCaducidad;

    public Producte(String nom, int preu, String marca, String fechaCaducidad) {
        this.nom = nom;
        this.preu = preu;
        this.marca = marca;
        this.fechaCaducidad = fechaCaducidad;
    }
    
}
